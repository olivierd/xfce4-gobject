#!/usr/bin/env python3
# -*- coding: utf-8 -*

'''
Display .desktop files (not nested) which are available in
"applicationsmenu" panel plugin.
'''

import sys

try:
    import gi

    gi.require_version('GLib', '2.0')
    gi.require_version('GObject', '2.0')
    gi.require_version('Gio', '2.0')
    gi.require_version('libxfce4util', '1.0')
    gi.require_version('Garcon', '1.0')
    from gi.repository import GLib, GObject, Gio, libxfce4util, Garcon
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


def main():
    menu = Garcon.Menu.new_applications()
    if menu.load(None):
        items = menu.get_elements()
        for item in items:
            if isinstance(item, Garcon.MenuItem):
                print(item.get_desktop_id())
    else:
        print('Error')
        sys.exit(-1)

if __name__ == '__main__':
    main()
