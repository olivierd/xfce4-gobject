#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Simple Python 3 script which shows how to use the org.xfce.Xfconf service.

For more details, look up https://git.xfce.org/xfce/xfconf/tree/common/xfconf-dbus.xml
'''

import sys

try:
    import gi

    gi.require_version('Gio', '2.0')
    from gi.repository import Gio
except ImportError:
    print('Additional module is required')
    sys.exit(-1)


def xfconf_list_channels(proxy):
    '''Return list of channels, as xfconf-query -l.'''

    list_channels = proxy.ListChannels()

    return list_channels

def xfconf_get_properties_from_channel(proxy, channel_name):
    '''As xfconf-query -c thunar -l'''

    props = proxy.GetAllProperties('(ss)', channel_name, '/')

    # It is dictionnary
    return props

def xfconf_get_property_value(proxy, channel, prop_name):
    '''As xfconf-query -c xsettings -p /Net/ThemeName'''
    val = False

    if prop_name.startswith('/'):
        if proxy.PropertyExists('(ss)', channel, prop_name):
            val = proxy.GetProperty('(ss)', channel, prop_name)

    return val

def main():
    conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(conn, Gio.DBusProxyFlags.NONE,
                                   None, 'org.xfce.Xfconf',
                                   '/org/xfce/Xfconf',
                                   'org.xfce.Xfconf', None)

    channels = xfconf_list_channels(proxy)
    if channels:
        if 'thunar' in channels:
            properties = xfconf_get_properties_from_channel(proxy,
                                                            'thunar')
            if properties:
                print(properties)

        if 'xsettings' in channels:
            res = xfconf_get_property_value(proxy, 'xsettings',
                                            '/Net/ThemeName')
            if res is not None:
                print('{0}'.format(res))
            else:
                print('Try another property')

    sys.exit(0)


if __name__ == '__main__':
    main()
