#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
POC how using Xfconf GObject introspection.

If everything is fine, you can remove new channel with following
command:
    xfconf-query -c xfce4-dummy -r -R -p /
'''

import sys

try:
    import gi
    gi.require_version('GObject', '2.0')
    gi.require_version('Gio', '2.0')
    gi.require_version('Xfconf', '0')
    from gi.repository import GObject, Gio, Xfconf
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


def get_all_properties(channel):
    proxy = Gio.DBusProxy.new_for_bus_sync(Gio.BusType.SESSION,
                                           Gio.DBusProxyFlags.NONE,
                                           None,
                                           'org.xfce.Xfconf',
                                           '/org/xfce/Xfconf',
                                           'org.xfce.Xfconf', None)
    props = proxy.GetAllProperties('(ss)', channel, '/')

    return props

def main():
    channel_name = 'xfce4-dummy'

    res = Xfconf.init()
    if res:
        channel = Xfconf.Channel.new(channel_name)

        # Create our first property → boolean type
        channel.set_property('/property-boolean', True)

        # Our 2nd property → string type
        val = GObject.Value(GObject.TYPE_STRING)
        val.set_string('hello')
        channel.set_property('/property-string', val)

        #result = get_all_properties(channel_name)
        result = channel.get_properties('/')
        if result:
            print(result)


if __name__ == '__main__':
    main()
    Xfconf.shutdown()
