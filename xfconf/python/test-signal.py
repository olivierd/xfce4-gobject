#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Use signal of Xfconf.Channel class.

Before to run this script, launch test-03.py first.
'''

import sys

try:
    import gi
    gi.require_version('GLib', '2.0')
    gi.require_version('GObject', '2.0')
    gi.require_version('Xfconf', '0')
    from gi.repository import GLib, GObject, Xfconf
except ImportError:
    print('Additional modules are required')
    sys.exit(-1)


def property_changed_cb(channel, prop_name, val):
    if channel.has_property(prop_name):
        print('{0}: {1}'.format(prop_name, val))

def main():
    res = Xfconf.init()
    if res:
        channel = Xfconf.Channel.new('xfce4-dummy')
        #print(GObject.signal_list_names(channel))

        channel.connect('property-changed', property_changed_cb)

        loop = GLib.MainLoop.new(None, True)
        loop.run()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('KeyboardInterrupt has been caught.')

    Xfconf.shutdown()
