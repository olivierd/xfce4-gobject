#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Another Python script, which shows how to use the org.xfce.Xfconf service.

We use the Gio.DBusProxy.call_sync() method, instead call explicit method.

For more details, look up https://git.xfce.org/xfce/xfconf/tree/common/xfconf-dbus.xml.
'''

import sys

try:
    import gi

    gi.require_version('GLib', '2.0')
    gi.require_version('Gio', '2.0')
    from gi.repository import GLib, Gio
except ImportError:
    print('Additional module is required')
    sys.exit(-1)

def xfconf_list_channels(proxy):

    result = []

    # val is GLib.Variant object
    val = proxy.call_sync('ListChannels', None,
                          Gio.DBusCallFlags.NONE,
                          -1, None)

    if val.n_children() == 1:
        # We can use unpack() method → it returns a tuple
        #result = val.unpack()[0]
        # Or get child at the specified index
        result = val.get_child_value(0)

    return result

def xfconf_get_property_value(proxy, channel, prop_name):
    '''As xfconf-query -c xsettings -p /Net/ThemeName'''
    result = None

    # Parameters of method
    args = GLib.Variant('(ss)', (channel, prop_name))

    val = proxy.call_sync('PropertyExists', args,
                          Gio.DBusCallFlags.NONE,
                          -1, None)
    # PropertyExists() → boolean
    if val.unpack()[0]:
        val = proxy.call_sync('GetProperty', args,
                              Gio.DBusCallFlags.NONE,
                              -1, None)

        result = val.unpack()[0]

    return result



def main():
    conn = Gio.bus_get_sync(Gio.BusType.SESSION, None)
    proxy = Gio.DBusProxy.new_sync(conn, Gio.DBusProxyFlags.NONE,
                                   None, 'org.xfce.Xfconf',
                                   '/org/xfce/Xfconf',
                                   'org.xfce.Xfconf', None)

    channels = xfconf_list_channels(proxy)
    if channels:
        if 'xsettings' in channels:
            res = xfconf_get_property_value(proxy, 'xsettings',
                                            '/Net/ThemeName')
            if res is not None:
                print('{0}'.format(res))
            else:
                print('Try another property')

    sys.exit(0)


if __name__ == '__main__':
    main()
