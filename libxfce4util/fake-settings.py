#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Simple example how to use the libxfce4util 1.0 API.

You must have libxfce4util >= 4.13.
'''

import os
import pathlib

import gi

gi.require_version('GLib', '2.0')
gi.require_version('GObject', '2.0')
gi.require_version('libxfce4util', '1.0')

from gi.repository import GLib, GObject, libxfce4util


def store_config(filename, seq):
    res = False

    # We build filename
    path = libxfce4util.resource_save_location(libxfce4util.ResourceType.CONFIG,
                                               filename, True)
    if path is not None:
        # We open 'filename' in read/write mode
        rc = libxfce4util.rc_simple_open(path, False)
        if isinstance(rc, libxfce4util.Rc):
            rc.set_group('Configuration')

            for k in iter(seq):
                value = seq.get(k)
                if isinstance(value, bool):
                    rc.write_bool_entry(k, value)
                elif isinstance(value, int):
                    rc.write_int_entry(k, value)
                else:
                    rc.write_entry(k, value)

            # Check if everything has been written
            rc.flush()
            if rc.is_dirty():
                print('Error')
            else:
                res = True

        rc.close()

    return res

def read_config(filename):
    '''We know keys in config file.'''
    path = libxfce4util.resource_lookup(libxfce4util.ResourceType.CONFIG,
                                        filename)
    if path is not None:
        # We open 'filename' in read mode
        rc = libxfce4util.rc_simple_open(path, True)
        if isinstance(rc, libxfce4util.Rc):
            grp = rc.get_group()
            if grp == 'Configuration':
                for i in rc.get_entries(grp):
                    if i == 'use-headerbar' or i == 'show-hidden-files':
                        print(rc.read_bool_entry(i, False))
                    elif 'window-' in i:
                        print(rc.read_int_entry(i, 0))
                    elif i == 'file-manager':
                        print(rc.read_entry(i, ''))

        rc.close()

def main():
    dummy_rc = os.path.join('xfce4', 'dummy.rc')
    default_settings = {'use-headerbar': False,
                        'show-hidden-files': False,
                        'window-width': 650,
                        'window-height': 470,
                        'file-manager': 'Thunar'}

    # We ensure, GObject introspection is available
    if float(libxfce4util.version_string()) >= 4.13:
        res = store_config(dummy_rc, default_settings)
        if res:
            read_config(dummy_rc)

        p = libxfce4util.resource_lookup(libxfce4util.ResourceType.CONFIG,
                                         dummy_rc)
        if p is not None:
            pathlib.Path(p).unlink()
    else:
        print('Noooo')


if __name__ == '__main__':
    main()
